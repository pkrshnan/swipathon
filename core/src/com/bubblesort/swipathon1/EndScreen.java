package com.bubblesort.swipathon1;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by pranavkrishnan on 2017-08-04.
 */

public class EndScreen implements GestureDetector.GestureListener, Screen {
    final Swipathon game;
    private SpriteBatch batch;
    private Sprite replay;
    private Sprite home;
    private Sprite volume;

    private int screenWidth;
    private int screenHeight;

    // Fonts
    FreeTypeFontGenerator generator;
    FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private BitmapFont title;
    private BitmapFont score;
    private BitmapFont highScore;

    // Nums
    private int scoreInt;
    private int highNum;
    private Boolean newHigh = false;

    private Texture bg;
    private Sprite background;

    // Camera
    OrthographicCamera camera;
    float height = 2000;
    float ppu = Gdx.graphics.getHeight() / height;
    float width = Gdx.graphics.getWidth() / ppu;


    public EndScreen(final Swipathon game) {
        this.game = game;

        batch = new SpriteBatch();
        scoreInt = GameScreen.scoreCount;
        highNum = Swipathon.prefs.getInteger("highscore", 0);

        // Setting up Screen Variables
        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2, height / 2, 0);
        camera.update();

        batch.setProjectionMatrix(camera.combined);

        if (scoreInt > highNum) {
            highNum = scoreInt;
            Swipathon.prefs.putInteger("highscore", scoreInt);
            Swipathon.prefs.flush();
            newHigh = true;
        }

        //Screen
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        // BG
        bg = new Texture("Background 1.png");
        background = new Sprite(bg);
        background.setSize(width, height);

        // Fonts
        title = SplashScreen.manager.get("size150.ttf", BitmapFont.class);
        score = SplashScreen.manager.get("size150.ttf", BitmapFont.class);
        highScore = SplashScreen.manager.get("size50.ttf", BitmapFont.class);

        // Replay
        Texture replayImg = new Texture("replay.png");
        replay = new Sprite(replayImg);
        replay.setPosition(width / 2 - 95f, 300);
        replay.setSize(190, 155);

        // Home
        Texture homeImg = new Texture("Home.png");
        home = new Sprite(homeImg);
        home.setPosition(width / 2 + 390 - 45, 335);
        home.setSize(90, 90);

        // Volume
        if (Swipathon.prefs.getBoolean("sound", true)) {
            volume = MainMenuScreen.changeVolume("Volume.png", width / 2 - 390 - 90, 305);
        } else {
            volume = MainMenuScreen.changeVolume("Mute.png", width / 2 - 390 - 90, 305);
        }
        volume.setSize(180, 135);

        // Gesture Detector
        GestureDetector gd = new GestureDetector(this);
        Gdx.input.setInputProcessor(gd);
    }


    @Override
    public void show() {
        if (Swipathon.prefs.getBoolean("sound", true)) {
            MainMenuScreen.music.play();
        }
    }


    @Override
    public void render(float delta) {
        batch.begin();

        background.draw(batch);
        replay.draw(batch);
        volume.draw(batch);
        home.draw(batch);
        title.draw(batch, "Game Over!", width / 2 - 350, height / 2 + 150);

        GlyphLayout glyphLayout = new GlyphLayout();
        String item = String.valueOf(scoreInt);
        glyphLayout.setText(score,item);
        float w = glyphLayout.width / 2;

        score.draw(batch, String.valueOf(scoreInt), width / 2 - w, height - 350);


        if (newHigh == false) {
            item = "Best: " + String.valueOf(highNum);
            glyphLayout.setText(highScore, item);
            w = glyphLayout.width / 2;
            highScore.draw(batch, "Best: " + String.valueOf(highNum), width / 2 - w, height - 550);
        } else {
            item = "New High Score!";
            glyphLayout.setText(highScore, item);
            w = glyphLayout.width / 2;
            highScore.draw(batch, "New High Score!", width / 2 - w, height - 550);
        }

        batch.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        MainMenuScreen.music.pause();
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        Vector3 v = new Vector3(x, y, 0);
        camera.unproject(v);

        float startHomeX = home.getX();
        float endHomeX = startHomeX + home.getWidth();

        if (v.x >= startHomeX && v.x <= endHomeX) {
            if (v.y >= home.getY() && v.y <= home.getY() + home.getHeight()) {
                scoreInt = -1;
                game.setScreen(new MainMenuScreen(game));
            }
        }
        else if (v.x >= replay.getX() && v.x <= replay.getX() + replay.getWidth()) {
            if (v.y >= replay.getY() && v.y <= replay.getY() + replay.getHeight()) {
                scoreInt = -1;
                game.setScreen(new GameScreen(game));
            }
        } else if (v.x >= (volume.getX()) && v.x <= (volume.getX() + volume.getWidth())) {
            if (v.y >= volume.getY() && v.y <= volume.getY() + volume.getHeight()) {
                if (MainMenuScreen.music.isPlaying()) {
                    volume = MainMenuScreen.changeVolume("Mute.png", width / 2 - 390 - 90, 305);
                    volume.setSize(180, 135);
                    Swipathon.prefs.putBoolean("sound", false);
                    Swipathon.prefs.flush();
                    MainMenuScreen.music.stop();
                } else {
                    volume = MainMenuScreen.changeVolume("Volume.png", width / 2 - 390 - 90, 305);
                    volume.setSize(180, 135);
                    MainMenuScreen.music.play();
                    Swipathon.prefs.putBoolean("sound", true);
                    Swipathon.prefs.flush();
                }
            }
        }
        return true;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
