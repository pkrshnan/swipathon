`package com.bubblesort.swipathon1;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.bubblesort.swipathon1.SplashScreen;
import com.bubblesort.swipathon1.Swipathon;

import java.util.Random;

public class GameScreen implements GestureListener, Screen{

	final Swipathon game;
	private SpriteBatch batch;

	// Pause
	State state = State.Running;
	private Sprite pause;
	private Sprite play;
	private Sprite replay;
	private Sprite home;
	private Sprite volume;

	// Sounds
	Sound kachingSound;

	// Graphics
	private Texture bg;
	private Sprite background;

	// UI
	private Sprite legend;

	// Colors
	private String red = "FF3300";
	private String blue = "0066FF";
	private String green = "92E124";
	private String orange = "F6881F";

	// Shape
	private int selector;
	private int color;

	//Score + Timer
	public static int scoreCount = -1;
	private int probability;
	private int labelCount;
	private BitmapFont score;
	private BitmapFont timeLeft;
	private BitmapFont label;
	private BitmapFont pauseText;

	// Tutorial
	private Sprite tutorial;
	private Sprite tutorialBut;

	// Screen
	OrthographicCamera camera;

	float height = 2000;
	float ppu = Gdx.graphics.getHeight() / height;
	float width = Gdx.graphics.getWidth() / ppu;

	// Timer
	private int timer = 150;
	private int frames = 0;
	private int timerReset = 150;
	private int timerFromCreate = 0;

	// Swiping
	private int swipeRightCount = 0;
	private int swipeLeftCount = 0;
	private int swipeUpCount = 0;
	private int swipeDownCount = 0;



	public enum State {
		Paused,
		Running,
		Tutorial
	}

	private float toFloat(double convert) {
		return (float) convert;
	}


	// Did Move Equivalent

	public GameScreen(final Swipathon game) {
		this.game = game;

		// Swipe Counts

		batch = new SpriteBatch();

		// Setting up Screen Variables
		camera = new OrthographicCamera(width, height);
		camera.position.set(width / 2, height / 2, 0);
		camera.update();

		batch.setProjectionMatrix(camera.combined);

		// Sounds
		kachingSound = Gdx.audio.newSound(Gdx.files.internal("Kaching.mp3"));

		// Background + Batch
		bg = new Texture("Background.jpg");
		background = new Sprite(bg);
		background.setSize(width * 1.2f, height * 1.2f);
		background.setPosition(-200, -200);

		// Legend
		Texture legendImg = new Texture("Legend.png");
		legend = new Sprite(legendImg);
		legend.setSize(750, 150);
		legend.setPosition(width/2 - 375f, 300);

		score = SplashScreen.manager.get("size150.ttf", BitmapFont.class);
		label = SplashScreen.manager.get("size100.ttf", BitmapFont.class);
		pauseText = SplashScreen.manager.get("size100.ttf", BitmapFont.class);
		timeLeft = SplashScreen.manager.get("size50.ttf", BitmapFont.class);

		// Pause
		Texture pauseImg = new Texture("pause.png");
		pause = new Sprite(pauseImg);
		pause.setPosition(width - 160, height - 160);
		pause.setSize(85f, 85f);

		// Replay
		Texture replayImg = new Texture("replay.png");
		replay = new Sprite(replayImg);
		replay.setPosition(width / 2 - 95f, 300);
		replay.setSize(190, 155);

		// Home
		Texture homeImg = new Texture("Home.png");
		home = new Sprite(homeImg);
		home.setPosition(width / 2 + 390 - 45, 335);
		home.setSize(90, 90);

		// Volume
		if (Swipathon.prefs.getBoolean("sound", true)) {
			volume = com.bubblesort.swipathon1.MainMenuScreen.changeVolume("Volume.png", width / 2 - 390 - 90, 305);
		} else {
			volume = com.bubblesort.swipathon1.MainMenuScreen.changeVolume("Mute.png", width / 2 - 390 - 90, 305);
		}
		volume.setSize(180, 135);


		// Play
		Texture playImg = new Texture("Play.png");
		play = new Sprite(playImg);
		play.setPosition(width - 160, height - 160);
		play.setSize(100, 100);

		// Tutorial
		Texture tutorialButImg = new Texture("Tutorial.png");
		tutorialBut = new Sprite(tutorialButImg);
		tutorialBut.setPosition(50 , height - 175);
		tutorialBut.setSize(110, 110);

		Texture tutorialImg = new Texture("Tut.png");
		tutorial = new Sprite(tutorialImg);
		tutorial.setPosition(0, 0);
		tutorial.setSize(width, height);

		// Gesture Detector
		GestureDetector gd = new GestureDetector(this);
		Gdx.input.setInputProcessor(gd);

		reset();

	}

	@Override
	public void show() {
		if (Swipathon.prefs.getBoolean("sound", true)) {
			Gdx.app.log("Works", "true");
			com.bubblesort.swipathon1.MainMenuScreen.music.play();
		}
		scoreCount = 0;

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
		com.bubblesort.swipathon1.MainMenuScreen.music.pause();
	}

	// Deleting Resources (Memory Management)
	@Override
	public void dispose() {
		bg.dispose();
		score.dispose();
		label.dispose();
		pauseText.dispose();
		timeLeft.dispose();

	}

	// Update Equivalent - addChilds here

	@Override
	public void render(float delta) {

		if (state == State.Running) {
			timer -= 1;
			frames += 1;
			timerFromCreate += 1;

			if(labelCount == 0){
				if(swipeDownCount > 0 || swipeLeftCount > 0 || swipeUpCount > 0 || swipeRightCount > 0) {
					Gdx.app.log("Error in Render", "labelCount == 0");
					exit();
				}
			}
			if (timer == 0 && labelCount == 0) {
				reset();
			}

			if (timer == 0) {
				Gdx.app.log("Error in Render", "timer == 0");
				exit();
			}

			if (frames % 1 == 0) {
				checkCount();
			}

			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			batch.begin();
			background.draw(batch);
			legend.draw(batch);
			pause.draw(batch);
			tutorialBut.draw(batch);

			GlyphLayout glyphLayout = new GlyphLayout();
			String item = String.valueOf(scoreCount);
			glyphLayout.setText(score,item);
			float w = glyphLayout.width / 2;

			score.draw(batch, String.valueOf(scoreCount), width / 2 - w, height - 350);
			timeLeft.draw(batch, String.valueOf(timer / 60) + ":" + String.valueOf(timer % 60 * (100 / 60)), width / 2 - 55, height - 550);

			batch.end();

			changeShape();

			batch.begin();
			label.draw(batch, String.valueOf(probability), width / 2 - 45, height / 2 + 45);
			batch.end();

		} else if (state == State.Paused) {
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			batch.begin();

			background.draw(batch);
			play.draw(batch);
			replay.draw(batch);
			home.draw(batch);
			volume.draw(batch);
			tutorialBut.draw(batch);


			GlyphLayout glyphLayout = new GlyphLayout();
			String item = String.valueOf(scoreCount);
			glyphLayout.setText(score,item);
			float w = glyphLayout.width / 2;

			score.draw(batch, String.valueOf(scoreCount), width / 2 - w, height - 350);
			timeLeft.draw(batch, String.valueOf(timer / 60) + ":" + String.valueOf(timer % 60 * (100 / 60)), width / 2 - 60, height - 550);

			item = "Paused";
			glyphLayout.setText(pauseText,item);
			w = glyphLayout.width / 2;
			pauseText.draw(batch, "Paused", width / 2 - w, height - 100);

			batch.end();

			changeShape();

			batch.begin();
			label.draw(batch, String.valueOf(probability), width / 2 - 45, height / 2 + 45);
			batch.end();
		} else if (state == state.Tutorial) {
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			batch.begin();

			background.draw(batch);
			legend.draw(batch);
			pause.draw(batch);
			tutorialBut.draw(batch);

			GlyphLayout glyphLayout = new GlyphLayout();
			String item = String.valueOf(scoreCount);
			glyphLayout.setText(score,item);
			float w = glyphLayout.width / 2;

			score.draw(batch, String.valueOf(scoreCount), width / 2 - w, height - 350);
			timeLeft.draw(batch, String.valueOf(timer / 60) + ":" + String.valueOf(timer % 60 * (100 / 60)), width / 2 - 60, height - 550);
			batch.end();
			changeShape();

			batch.begin();
			label.draw(batch, String.valueOf(probability), width / 2 - 45, height / 2 + 45);
			batch.end();

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			ShapeRenderer shapeRenderer = new ShapeRenderer();
			shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
			shapeRenderer.setColor(new Color(0, 0, 0, 0.7f));
			shapeRenderer.rect(0, 0, width, height);
			shapeRenderer.end();

			batch.begin();
			tutorial.draw(batch);
			batch.end();
			Gdx.gl.glDisable(GL20.GL_BLEND);

		}

	}

	private void reset() {
		Random rand = new Random();

		selector = rand.nextInt(5);
		color = rand.nextInt(4);

		swipeRightCount = 0;
		swipeDownCount = 0;
		swipeLeftCount = 0;
		swipeUpCount = 0;

		if (Swipathon.prefs.getBoolean("sound", true)) {
			long ting = kachingSound.play(1.0f);
		}

		probability = rand.nextInt(111);
		if (probability < 11) {
			probability = 0;
		}
        else if (probability < 41) {
			probability = 1;
		}
        else if (probability < 71) {
			probability = 2;
		}
        else if (probability < 101) {
			probability = 3;
		}
        else if (probability < 111) {
			probability = 4;
		}
		labelCount = probability;
		scoreCount += 1;

		if (scoreCount > 0) {
			if (scoreCount <  100) {
				if (scoreCount % 10 == 0 && timerReset > 90) {
					timerReset -= 10;
				}
			} else if (scoreCount > 100) {
				if (scoreCount % 10 == 0 && timerReset > 60) {
					timerReset -= 5;
				}
			}
		}

		timer = timerReset;

	}

	private void exit() {
		game.setScreen(new EndScreen(game));
	}

	private void checkCount() {
		if (color == 0) {
			if (swipeUpCount == labelCount && labelCount != 0)  {
				reset();
			}
			if (swipeRightCount > 0 || swipeDownCount > 0 || swipeLeftCount > 0) {
				Gdx.app.log("Error in Check", "color == 0");
				exit();
			}
		}
        else if (color == 1) {
			if (swipeLeftCount == labelCount && labelCount != 0) {
				reset();
			}
			if (swipeRightCount > 0 || swipeDownCount > 0 || swipeUpCount > 0) {
				Gdx.app.log("Error in Check", "color == 1");
				exit();
			}
		}
        else if (color == 2) {
			if (swipeDownCount == labelCount && labelCount != 0) {
				reset();
			}
			if (swipeRightCount > 0 || swipeUpCount > 0 || swipeLeftCount > 0) {
				Gdx.app.log("Error in Check", "color == 2");
				exit();
			}
		}
        else if (color == 3) {
			if (swipeRightCount == labelCount && labelCount != 0)  {
				reset();
			}
			if (swipeUpCount > 0 || swipeDownCount > 0 || swipeLeftCount > 0) {
				Gdx.app.log("Error in Check", "color == 3");
				exit();
			}
		}

	}

	private void changeShape() {

		String strokeColor = "";
		String fillColor = "";

		String[][] colors = {
				new String[]{"0036ff", "89c9ff"},
				new String[]{"FF8800", "FFE1A0"},
				new String[]{"11FF00", "B1FFBC"},
				new String[]{"FF0000", "FF9494"}
		};

		String startColor = colors[color][0];
		String endColor = colors[color][1];

		if (color == 0) {
			strokeColor = blue;
			fillColor = "Blue";
		} else if (color == 1) {
			strokeColor = orange;
			fillColor = "Orange";
		} else if (color == 2) {
			strokeColor = green;
			fillColor = "Green";
		} else if (color == 3) {
			strokeColor = red;
			fillColor = "Red";
		}

		Texture tex;
		if (selector == 0) {
			tex = new Texture(fillColor + "Square.png");
			Sprite shapeSprite = new Sprite(tex);

			batch.begin();
			batch.draw(shapeSprite, width / 2  - 200, height / 2 - 200, 400, 400);
			batch.end();

			tex.dispose();

		} else if (selector == 1) {
			tex = new Texture(fillColor + "Rhom.png");
			Sprite shapeSprite = new Sprite(tex);

			batch.begin();
			batch.draw(shapeSprite, width / 2  - 225, height / 2 - 225, 450, 450);
			batch.end();
			tex.dispose();


		} else if (selector == 2) {
			tex = new Texture(fillColor + "Tri.png");
			Sprite shapeSprite = new Sprite(tex);

			batch.begin();
			batch.draw(shapeSprite, width / 2  - 225, height / 2 - 180, 450, 375);
			batch.end();
			tex.dispose();

		} else if (selector == 3) {
			tex = new Texture(fillColor + "Circle.png");
			Sprite shapeSprite = new Sprite(tex);

			batch.begin();
			batch.draw(shapeSprite, width / 2  - 200, height / 2 - 200, 400, 400);
			batch.end();
			tex.dispose();

		} else if (selector == 4) {
			tex = new Texture(fillColor + "Hex.png");
			Sprite shapeSprite = new Sprite(tex);

			batch.begin();
			batch.draw(shapeSprite, width / 2  - 230, height / 2 - 200, 460, 400);
			batch.end();
			tex.dispose();

		}

	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		Vector3 v = new Vector3(x, y, 0);
		camera.unproject(v);

		if (state == state.Running) {
			if (v.x >= (pause.getX()) && v.x <= (pause.getX() + pause.getWidth())) {
				if (v.y >= (pause.getY()) && v.y <= (pause.getY() + pause.getHeight())) {
					state = State.Paused;
				}
			}
		}
		else if (state == state.Paused) {
			float startHomeX = home.getX();
			float endHomeX = startHomeX + home.getWidth();
			if (v.x >= (play.getX()) && v.x <= (play.getX() + play.getWidth())) {
				if (v.y >= (play.getY()) && v.y <= (play.getY() + play.getHeight())) {
					state = State.Running;
					timer += 10;
				}
			}
			else if (v.x >= startHomeX && v.x <= endHomeX) {
				if (v.y >= home.getY() && v.y <= home.getY() + home.getHeight()) {
					scoreCount = -1;
					game.setScreen(new com.bubblesort.swipathon1.MainMenuScreen(game));
				}
			}
			else if (v.x >= replay.getX() && v.x <= replay.getX() + replay.getWidth()) {
				if (v.y >= replay.getY() && v.y <= replay.getY() + replay.getHeight()) {
					scoreCount = -1;
					reset();
					state = State.Running;
				}
			} else if (v.x >= (volume.getX()) && v.x <= (volume.getX() + volume.getWidth())) {
				if (v.y >= volume.getY() && v.y <= volume.getY() + volume.getHeight()) {
					if (com.bubblesort.swipathon1.MainMenuScreen.music.isPlaying()) {
						volume = com.bubblesort.swipathon1.MainMenuScreen.changeVolume("Mute.png", width / 2 - 390 - 90, 305);
						volume.setSize(180, 135);
						Swipathon.prefs.putBoolean("sound", false);
						Swipathon.prefs.flush();
						com.bubblesort.swipathon1.MainMenuScreen.music.stop();
					} else {
						volume = com.bubblesort.swipathon1.MainMenuScreen.changeVolume("Volume.png", width / 2 - 390 - 90, 305);
						volume.setSize(180, 135);
						com.bubblesort.swipathon1.MainMenuScreen.music.play();
						Swipathon.prefs.putBoolean("sound", true);
						Swipathon.prefs.flush();
					}
				}
			}
		}

		else if (state == State.Tutorial) {
			state = State.Running;
		}

		if (v.x >= tutorialBut.getX() && v.x <= tutorialBut.getX() + tutorialBut.getWidth()) {
			if (v.y >= tutorialBut.getY() && v.y <= tutorialBut.getY() + tutorialBut.getHeight()) {
				state = State.Tutorial;
			}
		}

		return true;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		if (state == State.Running && timerFromCreate > 10) {
			if (Math.abs(velocityX) > Math.abs(velocityY)) {
				if (velocityX > 0) {
					onRight();
				} else {
					onLeft();
				}
			} else {
				if (velocityY > 0) {
					onDown();
				} else {
					onUp();
				}
			}
		}
		return true;
	}

	public void onUp() {
		swipeUpCount += 1;

		if (color == 0) {
			probability -= 1;
		}
	}

	public void onRight() {
		swipeRightCount += 1;

		if (color == 3) {
			probability -= 1;
		}
	}

	public void onLeft() {
		swipeLeftCount += 1;

		if (color == 1) {
			probability -= 1;
		}
	}

	public void onDown() {
		swipeDownCount += 1;

		if (color == 2) {
			probability -= 1;
		}
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {}

}
