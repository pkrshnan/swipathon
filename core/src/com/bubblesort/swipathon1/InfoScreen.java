package com.bubblesort.swipathon1;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


class InfoScreen implements Screen, GestureListener {

    final Swipathon game;

    // Background
    private SpriteBatch batch;
    private Sprite background;
    private Sprite exit;

    // Camera
    OrthographicCamera camera;

    float height = 20;
    float ppu = Gdx.graphics.getHeight() / height;
    float width = Gdx.graphics.getWidth() / ppu;


    public InfoScreen(final Swipathon game) {
        this.game = game;

        batch = new SpriteBatch();

        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2, height / 2, 0);
        camera.update();

        batch.setProjectionMatrix(camera.combined);

        // Background
        Texture bg = new Texture("InfoPage.png");
        background = new Sprite(bg);
        background.setSize(width, height);

        //Exit
        Texture exitTex = new Texture("Close.png");
        exit = new Sprite(exitTex);
        exit.setPosition(width - 2, height -2 );
        exit.setSize(1.7f, 1.4f);

        GestureDetector gd = new GestureDetector(this);
        Gdx.input.setInputProcessor(gd);
    }

    @Override
    public void show() {
        if (Swipathon.prefs.getBoolean("sound", true)) {
            com.bubblesort.swipathon1.MainMenuScreen.music.play();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.draw(batch);
        exit.draw(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        com.bubblesort.swipathon1.MainMenuScreen.music.pause();
    }

    @Override
    public void dispose() {}

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        Vector3 v = new Vector3(x, y, 0);
        camera.unproject(v);

        if (v.x >= (exit.getX()) && v.x <= (exit.getX() + exit.getWidth())) {
            if (v.y >= (exit.getY()) && v.y <= (exit.getY() + exit.getHeight())) {
                game.setScreen(new com.bubblesort.swipathon1.MainMenuScreen(game));
            }
        }

        return true;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {

        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
