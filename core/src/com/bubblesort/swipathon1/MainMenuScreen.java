package com.bubblesort.swipathon1;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


class MainMenuScreen implements Screen, GestureListener {

    final Swipathon game;
    public static Music music;
    OrthographicCamera camera;

    // Background
    private Texture bg;
    private SpriteBatch batch;
    private Sprite background;

    // Title
    private Texture titleTex;
    private Sprite title;

    // Icons
    private Sprite volume;
    private Sprite info;

    //Screen
    float height = 20;
    float ppu = Gdx.graphics.getHeight() / height;
    float width = Gdx.graphics.getWidth() / ppu;



    public MainMenuScreen(final Swipathon game) {
        this.game = game;

        batch = new SpriteBatch();

        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2, height / 2, 0);
        camera.update();

        batch.setProjectionMatrix(camera.combined);


        // Background
        bg = new Texture("Background 1.png");
        background = new Sprite(bg);
        background.setSize(width, height);

        // Title
        titleTex = new Texture("Swipathon.png");
        title = new Sprite(titleTex);
        title.setPosition(width / 2 - 5.5f, 10);
        title.setSize(11, 3f);

        // Icons
        if (Swipathon.prefs.getBoolean("sound", true)) {
            volume = changeVolume("Volume.png", width / 2 + 1.1f, 4);
        } else {
            volume = changeVolume("Mute.png", width / 2 + 1.1f, 4);
        }

        Texture infoTex = new Texture("Info.png");
        info = new Sprite(infoTex);
        info.setPosition(width / 2 - 2.5f, 4);
        info.setSize(1.4f, 1.2f);

        GestureDetector gd = new GestureDetector(this);
        Gdx.input.setInputProcessor(gd);
    }

    public static Sprite changeVolume(String icon, float x, float y) {

        Texture volumeTex = new Texture(icon);
        Sprite volume = new Sprite(volumeTex);
        volume.setPosition(x, y);
        volume.setSize(1.4f, 1.2f);
        return volume;
    }

    @Override
    public void show() {
        MainMenuScreen.music = Gdx.audio.newMusic(Gdx.files.internal("bgm.wav"));
        MainMenuScreen.music.setLooping(true);
        if (Swipathon.prefs.getBoolean("sound", true)) {
            MainMenuScreen.music.play();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        background.draw(batch);
        title.draw(batch);
        volume.draw(batch);
        info.draw(batch);

        batch.end();

    }

    @Override
    public void resize(int width, int height) { }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        MainMenuScreen.music.pause();
    }

    @Override
    public void dispose() {
        titleTex.dispose();
        bg.dispose();
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {


        Vector3 v = new Vector3(x, y, 0);
        camera.unproject(v);

        if (info.getX() < v.x && info.getX() + info.getWidth() > v.x) {
            if (info.getY() < v.y && info.getY() + info.getHeight() > v.y) {
                game.setScreen(new InfoScreen(game));
            } else {
                game.setScreen(new GameScreen(game));
            }
        } else if (v.x >= (volume.getX()) && v.x <= (volume.getX() + volume.getWidth())) {
            if (v.y >= (volume.getY()) &&  v.y <= (volume.getY() + volume.getHeight())) {
                if (MainMenuScreen.music.isPlaying()) {
                    volume = changeVolume("Mute.png", width / 2 + 1.1f, 4);
                    MainMenuScreen.music.stop();
                    Swipathon.prefs.putBoolean("sound", false);
                    Swipathon.prefs.flush();
                } else {
                    volume = changeVolume("Volume.png", width / 2 + 1.1f, 4);
                    MainMenuScreen.music.play();
                    Swipathon.prefs.putBoolean("sound", true);
                    Swipathon.prefs.flush();
                }
            } else {
                game.setScreen(new GameScreen(game));
            }
        } else {
            game.setScreen(new GameScreen(game));
        }

        return true;
    }


    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
