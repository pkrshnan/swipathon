package com.bubblesort.swipathon1;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by pranavkrishnan on 2017-08-04.
 */

public class Swipathon extends Game {
    public static Preferences prefs;


    public void create() {
        prefs = Gdx.app.getPreferences("My Preferences");
        setScreen(new SplashScreen(this));

    }

    public void render() {
        super.render(); //important!
    }

    public void dispose() {
    }
}
